//
//  CustomHeaderView.h
//  Lesson10
//
//  Created by iOS-School-2 on 29.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *label;

+ (UINib *)nib;
@end
