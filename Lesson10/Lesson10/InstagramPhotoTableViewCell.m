//
//  InstagramPhotoTableViewCell.m
//  Lesson10
//
//  Created by iOS-School-2 on 26.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "InstagramPhotoTableViewCell.h"

@interface InstagramPhotoTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoUIImageView;
@property (weak, nonatomic) IBOutlet UIView *likeView;

@end


@implementation InstagramPhotoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setHighlighted:(BOOL)highlighted {
    //когда подсвечена
}

-(void)setInstagramPhoto:(InstagramPhoto *)photo {
    self.userNameLabel.text = photo.userName;
    self.photoUIImageView.image = photo.image;
    self.likeView.backgroundColor = photo.isLike ? [UIColor redColor] : [UIColor grayColor];
}

- (IBAction)buttonDidTap:(id)sender {
    if ([self.delegate respondsToSelector:@selector(cellDidTapButton:)]) {
        [self.delegate cellDidTapButton:self];
    }
}


@end
