//
//  InstagramPhoto.h
//  Lesson10
//
//  Created by iOS-School-2 on 26.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@interface InstagramPhoto : NSObject

@property (nonatomic, copy)NSString *userName;
@property (nonatomic, copy)UIImage *image;
@property (nonatomic, assign)BOOL isLike;

@end
