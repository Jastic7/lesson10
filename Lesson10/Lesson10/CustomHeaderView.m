//
//  CustomHeaderView.m
//  Lesson10
//
//  Created by iOS-School-2 on 29.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "CustomHeaderView.h"

@implementation CustomHeaderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+(UINib *)nib {
    return [UINib nibWithNibName:@"CustomHeaderView" bundle:nil];
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectZero];
    v.backgroundColor = [UIColor blueColor];
    self.backgroundView = v;
}
@end
