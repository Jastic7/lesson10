//
//  InstagramPhotoTableViewCell.h
//  Lesson10
//
//  Created by iOS-School-2 on 26.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramPhoto.h"

@class InstagramPhotoTableViewCell;

@protocol InstagramPhotoTableViewCellDelegate <NSObject>

- (void)cellDidTapButton:(InstagramPhotoTableViewCell *)cell;

@end

@interface InstagramPhotoTableViewCell : UITableViewCell

@property (nonatomic, weak) id<InstagramPhotoTableViewCellDelegate> delegate;
- (void)setInstagramPhoto:(InstagramPhoto *)photo;

@end
