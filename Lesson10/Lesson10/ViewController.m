//
//  ViewController.m
//  Lesson10
//
//  Created by iOS-School-2 on 26.04.17.
//  Copyright © 2017 iOS-School-2. All rights reserved.
//

#import "ViewController.h"
#import "InstagramPhotoTableViewCell.h"
#import "TextTableViewCell.h"
#import "CustomHeaderView.h"

static NSString * const kCellID = @"InstagramPhotoTableViewCellID";
static NSString * const kTextCellID = @"TextTableViewCellID";
static NSString * const kHeaderViewID = @"CustomHeaderView";

@interface ViewController () <UITableViewDataSource, InstagramPhotoTableViewCellDelegate, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *models;
@property (nonatomic, strong) NSArray *headers;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[CustomHeaderView nib] forHeaderFooterViewReuseIdentifier:kHeaderViewID];
    self.tableView.sectionHeaderHeight = 30;
    
    //если реализован делегат для отображения высоты - то это важно!
    //вместо 500 вызовов метода делегата, он вызовестя 20 раз, например, а для остальных будет использовать estimated
    self.tableView.estimatedRowHeight = 200;
    
//    self.tableView.rowHeight = 250;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.headers = @[@"First header", @"Second header", @"Third header"];
    self.models = [NSMutableArray new];
    
    NSMutableArray *section1 = [@[] mutableCopy];
    [section1 addObject:@"Родился в 1954 году в Могилёве, в тюрьме[2], рос без отца, мать, Геня Борисовна Моисеева (Мойсес), была политзаключённой[3]. Памяти матери после её смерти Борис посвятил песню «Глухонемая любовь», так как она была убита глухонемым, который ошибся дверью. Окончив школу, уехал в Минск, где поступил в хореографическое училище. Учился у балерины Нины Млодзинской. Закончил училище как классический танцовщик"];
    
    [section1 addObject:@"В 1987 году трио вышло из труппы Пугачёвой и начало сольную карьеру. C 1988 по 1989 год «Экспрессия» выступала в клубах Италии, Франции и Америки. Трио долгое вра-постановщика муниципального театра города Новый Орлеан."];
    
    [section1 addObject:@"В 1994 году выходит шоу-программа «Каприз Бориса Моисеева». В 1995 году вышел спектакль «Дитя порока». В 1996 году Моисеев выпускает спектакль «Падший ангел». По словам Моисеева, это спектакль-исповедь, где чётко прослеживается его собственная судьба: «Я пою о трагедии и о любви. Я всегда показываю глубину человеческих чувств, кто бы ни был ими связан — мужчина и женщина, мужчина и мужчина. Это любовь со всеми её взлётами и падениями. Я играю на сцене историю чувств».[источник не указан 2176 дней] В 1998 году спектакль был показан на Бродвее в театре «Beacon Theatre». В 1997 году Моисеев выпускает спектакль «Королевство любви», в 1999 — шоу «25 лет на сцене или Просто Щелкунчик»."];
    
    [section1 addObject:@"В 2008 году в дуэте с Еленой Воробей участвует в конкурсной программе «Две звезды» (второй сезон)[7]."];
    
    NSMutableArray *section2 = [@[] mutableCopy];
    [section2 addObject:@"Родился в 1954 году в Могилёве, рафическое училище. Учился у балерины Нины Млодзинской. Закончил училище как классический танцовщик"];
    
    [section2 addObject:@"В 1987 году трио вышло из труппы Пугачёвой и начало сольную карьеру. C 1988 по 1989 год «Экспрессия» выступала в клубах Италии, Франции и Америки. Трио долгое вра-постановщика муниципального театра города Новый Орлеан."];
    
    [section2 addObject:@"В 1994 году выходит шоу-программа «Каприз Бориса Моисеева». В 1995 году вышел спектакль «Дитя порока». В 1996 году Моисеев выпускает спектакль «Падший ангел». Пектакль «Королевство любви», в 1999 — шоу «25 лет на сцене или Просто Щелкунчик»."];
    
    [section2 addObject:@"20 декабря 2010 года Борис Моисеев госпитализирован с подозрением на инсульт, 21 декабря лечащие врачи подтвердили диагноз. С каждым днём состояние артиста ухудшалось, в результате чего левая часть тела была парализована. 23 декабря певец был введён в кому, ему подключили аппарат искусственной вентиляции лёгких. 3 февраля 2011 артист был выписан из больницы и находился у себя дома[9], 17 апреля он появился на концерте у Кристины Орбакайте, а уже 22 июля пел на юрмальской «Новой волне». Вместе с тем, до конца он не реабилитировался: у него работают не все мышцы лица и затруднена речь[4]"];
    
    [self.models addObject:section1];
    [self.models addObject:section2];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *title = self.headers[section];
    CustomHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:kHeaderViewID];
    headerView.label.text = title;
    
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *sections = self.models[indexPath.section];
    InstagramPhoto *photo = sections[indexPath.row];
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return self.headers[section];
//}

//НАИВЫСШИЙ ПРИОРИТЕТ. Будет использоваться этот метод, если он есть.
//кастомный размер ячеек
// срабатывает, конда таблица перезанружается
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    //тут мы не знаем какой размер нм нужен, чтобы вместить весь текст
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
//    label.numberOfLines = 0;
//    label.font = [UIFont systemFontOfSize:17];
//    NSArray *section = self.models[indexPath.section];
//    label.text = section[indexPath.row];
//    
//    //2*10 это отступы
//    //+1 это для правильного округления
//    return [label sizeThatFits:CGSizeMake(355, CGFLOAT_MAX)].height + 2 * 10 + 1;
//}

#pragma mark - UITableViewDataSource


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.models.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = self.models[section];
    return sections.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTextCellID forIndexPath:indexPath];
    NSArray *section = self.models[indexPath.section];
    cell.label.text = section[indexPath.row];
    return cell;
    
}

#pragma mark - InstagramPhotoTableViewCellDelegate
-(void)cellDidTapButton:(InstagramPhotoTableViewCell *)cell {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Alert" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
